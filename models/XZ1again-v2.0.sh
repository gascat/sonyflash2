#!/bin/bash
zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	command=$(/bin/bash ~/Sony_Flasher/Models/XZ1flash_v2.0.sh &)
else
	command=$(rm -r ~/local/G3121_Customized_UK_*)
	zenity --error --text="Local Files Removed"
	exit
fi
/bin/bash XZ1again_v2.0.sh
sleep 5
