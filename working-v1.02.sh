#!/bin/bash

#make temp dir
mkdir ~/zentemp

#list folder in share to text file array
ls ~/Sony_share >> ~/zentemp/folders.txt

sed -e 's/$/\nmodel/' -i /root/folders.txt

#read text file into array
modString=$(cat  ~/zentemp/folders.txt |tr "\n" " " )

mod=($modString)

#build zenity interface
ans=$(zenity --list --text "Sony Flasher" --radiolist --column "pick" --column "Model" TRUE "${mod[1]}" FALSE "${mod[@]}" );

/bin/bash $ans.sh

rm -r ~/zentemp/*
rm ~/zentemp
